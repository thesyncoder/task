import firebase from 'firebase';
const config = {
    apiKey: "AIzaSyDtX6XWrQGh57D8JAFqnUpGZyH007_na9w",
    authDomain: "tasks-82a7b.firebaseapp.com",
    databaseURL: "https://tasks-82a7b.firebaseio.com",
    projectId: "tasks-82a7b",
    storageBucket: "tasks-82a7b.appspot.com",
    messagingSenderId: "61496582710",
    appId: "1:61496582710:web:83680899284860e51fee97",
    measurementId: "G-JCT5NR1SG5"
};
firebase.initializeApp(config);
firebase.analytics();
const db = firebase.firestore();
export {
    firebase, db
};
