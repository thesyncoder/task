import React, { Component , useState , useEffect} from 'react';
import SidebarOn from "../components/sidebar"
import { Typography, Button } from '@material-ui/core';
import ProjectDetail from "../components/ProjectDetail";

import {firebase , db} from ".././firestore";

const  ProjectPage= (...props) => {

    const [workSpace , setWs] = useState('');
    const [Id , setId] = useState('');
    const [projects , setproject ] =useState([]);
    var ref =null;
    useEffect(()=>{
        console.log('useeffect triggered' , workSpace ,' <- workspace');
        setproject([]);
        if (workSpace !== '' ){
            ref = db.collection("workspaces").doc(Id);
           ref.get()
            .then( async(snapshot) =>{
                if (snapshot.exists){
                    
                    var data = snapshot.data().projects;
                    
                    console.log(data);
                    if (data !== undefined) await setproject(data);
                    console.log(projects);
                }
            })
        }
    },[workSpace ,setWs]);

    
    return (
      <div>
        <SidebarOn callback={setWs} callId={setId} />
        {workSpace !== "" ? (
          
            <div style={{ marginLeft: "300px" }}>
              <Typography variant="h3">{workSpace}</Typography>
              {
                 projects === [] ? <Typography variant="h3">No project yet in {workSpace} , add a project !</Typography> : <div></div>
              }
              
              <ProjectDetail projects={projects} callback={setproject} id={Id} props={props}/>
            </div>       
          
        ) : (
          <div style={{ marginLeft: "300px", padding: "2em" }}>
            <Typography variant="h3">
              Select a workspace or create one to continue
            </Typography>
          </div>
        )}
      </div>
    );
}

export default ProjectPage;
