import React , {useState} from 'react';
import {firebase, db} from './firestore';
// import SidebarOn from "./components/sidebar";
import "semantic-ui-css/semantic.min.css";
import ProjectPage from "./pages/projectPage";
import TaskPage from  "./pages/taskPage";
import {
  HashRouter,
  Route,
  Redirect,
  Switch,
  Link,
  withRouter,
} from "react-router-dom";


function App() {
  
  
  return (
    <React.Fragment>
      <HashRouter basename="/">
        <Switch>
          <Route
            path="/"
            exact
            render={(props) => <ProjectPage {...props} />}
          />
          <Route path="/task/:proid/:taskid" exact render={(props) => <TaskPage {...props} />} />

        </Switch>
      </HashRouter>
    </React.Fragment>
  );
}

export default App;
