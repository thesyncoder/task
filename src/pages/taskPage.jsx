import React, { Component , useState , useEffect } from 'react';
import { firebase , db} from "../firestore";
import DatePicker from "react-datepicker";
import { Typography, Button } from "@material-ui/core";
import "froala-editor/js/froala_editor.pkgd.min.js";

// Require Editor CSS files.
import "froala-editor/css/froala_style.min.css";
import "froala-editor/css/froala_editor.pkgd.min.css";

// Require Font Awesome.
// import "font-awesome/css/font-awesome.css";

import FroalaEditor from "react-froala-wysiwyg";


import styled from "styled-components";
const Item = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 1em;
`;

const Input = styled.input`
  border: none;
`;



const TaskPage =(props) => {
    const [id , setid] = useState( props.location.state['id']);
    const [ws , setws] = useState('');
    const [taskid , settid] = useState( props.match.params.taskid);
    const [ pro , setpro] = useState( '');
    const [proid , setpid] = useState( props.match.params.proid);
    const [task , settask] = useState([]);
    const [loaded ,setloaded] = useState(false);
    const [model, setModel] = useState("");

    const Menu = () => {
        const handleChange = obj => e => {
            var narr = [...task];
            narr[taskid][obj] = e.target.value;
            settask(narr);

            var nr = [...pro];
            var o = {};
            narr.forEach((e, i) => {
              o[i] = e;
            });
            nr[proid].tasks = o;
            setpro(nr);
            db.collection("workspaces").doc(id).update({
              projects: pro,
            });

        }
        const ChangeDate =(date) =>{
            var narr = [...task];
            narr[taskid]['duedate'] = date;
            settask(narr);

            var nr = [...pro];
            var o = {};
            narr.forEach((e, i) => {
              o[i] = e;
            });
            nr[proid].tasks = o;
            setpro(nr);
            db.collection("workspaces").doc(id).update({
              projects: pro,
            });
        }
        return (
          <div style={{display:"flex" , flexDirection:"row"}}>
            <Item>
              <Typography>Approver</Typography>
              <Input
                placeholder="Assign a approver"
                type="email"
                value={task[taskid].owner}
                onChange={handleChange("owner")}
              ></Input>
            </Item>
            <Item>
              <Typography>Due Date</Typography>
              <DatePicker
                dateFormat="yyyy/MM/dd"
                placeholderText="Click to select a date"
                selected={Date.parse(task[taskid].duedate)}
                onChange={ChangeDate}
              ></DatePicker>
            </Item>
            <Item>
              <Typography>Priority</Typography>
              <select
                placeholder="choose a status"
                value={task[taskid].priority}
                onChange={handleChange("priority")}
              >
                <option>High</option>
                <option>Medium</option>
                <option>Low</option>
              </select>
            </Item>
            <Item>
              <Typography>Status</Typography>
              <select
                placeholder="choose a status"
                value={task[taskid].status}
                onChange={handleChange("status")}
              >
                <option>Paused</option>
                <option>On progress</option>
                <option>Completed</option>
              </select>
            </Item>
          </div>
        );
    };

    useEffect(()=>{
            // console.log(props);
            settask([]);
            setpro([]);
            setloaded(false);
            db.collection('workspaces').doc(id).get()
            .then( async(snapshot) => {
                var data = await snapshot.data();
                setws(data.name);
                var tsk = data.projects[proid].tasks;
                if ( tsk[taskid].content !== undefined) setModel(tsk[taskid].content);
                var d =Object.values(tsk);
                setpro(data.projects);
                settask(d);
                setloaded(true);
            })

    }, []);
    const handleChange = obj => e => {
        var narr = [...task];
        narr[taskid][obj] = e.target.value;
        settask(narr);

        var nr = [...pro];
        var o = {};
        narr.forEach((e, i) => {
          o[i] = e;
        });
        nr[proid].tasks = o;
        setpro(nr);
        db.collection("workspaces").doc(id).update({
          projects: pro,
        });
    }
    
    const handleModelChange = (e) => {
        
        setModel(e);
        var narr = [...task];
        narr[taskid]['content'] = model.toString();
        settask(narr);

        var nr = [...pro];
        var o = {};
        narr.forEach((e, i) => {
          o[i] = e;
        });
        nr[proid].tasks = o;
        setpro(nr);
        db.collection("workspaces").doc(id).update({
          projects: pro,
        });
    }
    return (
      loaded && (
        <div style={{ margin: "1em", padding: "1em" }}>
          <Typography variant="h3" style={{ padding: "1em" }}>
            {ws}
          </Typography>
          <Input
            placeholder="Task name"
            type="text"
            value={task[taskid].task}
            onChange={handleChange("task")}
          ></Input>
          <Menu style={{ padding: "2em" }} />
          <div style={{ padding: "3em" }}></div>
          <FroalaEditor
            model={model}
            onModelChange={handleModelChange}
          />
        </div>
      )
    );
};

export default TaskPage;