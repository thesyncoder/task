import React from "react";
import styled from "styled-components";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import AddIcon from "@material-ui/icons/Add";
import NoteIcon from "@material-ui/icons/Note";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import {firebase , db} from "../firestore";


function rand() {
  return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const SidebarStyled = styled.div`
  position: fixed;
  z-index: 555;
  top: 0;
  left: 0;
  width: 80%;
  background-color: #fcedf7;
  padding: 1rem;
  color: black;
  max-width: 270px;
  height: 100%;
  transform: translateX(${(props) => (props.show ? "0" : "-100%")});
  transition: all 0.3s ease-in-out;
`;

const SidebarWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
`;

const Link = styled.a`
  text-decoration: none;
  color: #fff;
  font-family: inherit;
  padding: 1em 2rem;
  font-size: 13px;

  &:first-of-type {
    margin-top: 50px;
  }
`;

const Workspaces = styled.div`
  margin-top:1em;
  padding-left:1rem;
  padding-right:1rem;
  text-decoration:none;
  color:black;
  font-family:inherit;
  display:flex;
  flexDirection:row;
  justify-content:space-between; 
`;

const CloseIcon = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  cursor: pointer;
  padding: 10px 35px 16px 0px;

  & span,
  & span:before,
  & span:after {
    cursor: pointer;
    border-radius: 1px;
    height: 3px;
    width: 30px;
    background: white;
    position: absolute;
    display: block;
    content: "";
  }

  & span {
    background-color: transparent;
  }

  & span:before {
    top: 0;
    transform: rotate(45deg);
  }

  & span:after {
    top: 0;
    transform: rotate(-45deg);
  }
`;

const Item = styled.div`
  margin-top: 1em;
  padding: 1rem;

  text-decoration: none;
  color: black;
  font-family: inherit;
  display: flex;
  flexdirection: row;
  justify-content: center;
  border-radius: 10px;

  background: ${(props) => (props.set ? "#d6d2d5" : "transparent")};

  &:hover {
    border-radius: 10px;
    background: #e6e3e5;
  }
`;

const MenuItem = ( {item , id,  c , callback, callId} ) => {
  const [show ,setshow] = React.useState(c);
  return(
  <Item set={show} onClick={(e) => {
    callback(item)
    callId(id)
    setshow(true)
    }}>
    <Icon>
      <NoteIcon
        style={{ height: "20px", width: "20px", paddingRight: "5px" }}
      />
    </Icon>
    <Typography>{item}</Typography>
  </Item>
  )
}


const SidebarOn = ({ callback ,callId,show=1, setIsOpened=true }) => {
  const props='set';
  const [ws , setws] = React.useState([]);
  const [id, setid] =React.useState('');


  React.useEffect(()=>{
    db.collection('workspaces')
    .onSnapshot( snapshot => {

      if (snapshot.size){
        var nr=[];
        snapshot.forEach( doc => {
          nr.push({ name:doc.data().name,
            id :doc.id
          });
        });
        setws( nr );  
      }
      else{
        setws([]);
      }
      
    });
  },[]);

  const Menu =() => {
    return ws.map( (e ,i) => <MenuItem
      item={e.name}
      id={e.id}
      key={i}
      c={false}
      callback={callback}
      callId={callId}
    />
     )
  }

  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  }
  const handleOpen =() => {
    setOpen(true);
  }
   const [bb ,setbb] = React.useState('');
   const body = (

     <div style={modalStyle} className={classes.paper}>
       <h2 id="simple-modal-title">Add a workspace</h2>
       <input
        placeholder="workspace name"
        value ={bb}
        onChange={(e) => setbb(e.target.value) }
       ></input>
       <div style={{margin:"1em"}}></div>
       <Button variant="outlined" size="medium" 
       color="primary"
       onClick={() => {
         db.collection('workspaces').add({
           name:bb
         });
         setbb('');
         setOpen(false)
        }}
       >Submit</Button>
       
     </div>
   );
  return (
    <SidebarStyled show={show ? 1 : 0}>
      <SidebarWrapper>
        <div style={{ display: "flex", flexDirection: "row", margin: "1em" }}>
          <Icon fontSize="large">
            <AssignmentIcon style={{ height: "30px", width: "30px" }} />
          </Icon>
          <Typography variant="h4">Assign Tasks</Typography>
        </div>
        <Workspaces>
          <Typography variant="h6">Workspaces</Typography>
          <Icon>
            <AddIcon style={{ height: "25px", width: "25px" }} onClick={handleOpen}/>
          </Icon>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
          >
            {body}
          </Modal>
        </Workspaces>
        <hr style={{ border: "1px solid black", width: "200px" }} />
        <Menu/>
      </SidebarWrapper>
    </SidebarStyled>
  );
};

export default SidebarOn;
