import React, { Component, useState } from 'react';
import styled from "styled-components";
import { Typography ,Button , } from '@material-ui/core';
import Icon from "@material-ui/core/Icon";
import {  Label, Menu, Table } from 'semantic-ui-react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { makeStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import IconButton from "@material-ui/core/IconButton";
import { firebase , db } from "../firestore";
import clsx from "clsx";
import moment from 'moment';
import {withRouter} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
}));

const Divider = styled.hr`
    border:1px solid black;
    width:100%;
    margin:"2em"
`;

const Input = styled.input`
    border:none;
`;

const Item = styled.div`
    display:flex;
    flex-direction:column;
    align-items:center;
    justify-content:center;
    padding:1em;
`;





const Task =({id , index, props}) => {
    const [tasks , setTask ] = useState([]);
    const [show , setshow] =useState(false);
    const [ pro , setpro] = useState([]);
    var ref = null;
    React.useEffect(()=>{
        setTask([]);
        ref = db.collection('workspaces').doc(id);
        ref.get().then(async(snapshot) => {
          if (snapshot.exists) {
            var data = await snapshot.data();
            if ( data.projects !== undefined){
            setpro(data.projects);
            
            console.log(data.projects[index].tasks);
            if (data.projects[index].tasks !== undefined ){
            var d = data.projects[index].tasks;
            var val = Object.values(d);
            console.log(val)
            setTask(val);
            }
          }
            
            
          }
        });
    },[id])
    const Change = (i,obj) =>  (e) =>{
        var narr = [...tasks];
        narr[i][obj] = e.target.value;
        setTask(narr);
        
        var nr = [...pro];
        var o = {};
        narr.forEach((e, i) => {
          o[i] = e;
        });
        nr[index].tasks = o;
        setpro(nr);
        db.collection('workspaces').doc(id).update({
          projects : pro
        })
        
    } 
    const ChangeDate = i => date =>{
        var narr = [...tasks];
        narr[i]["duedate"] = date.toLocaleDateString();
        setTask(narr);
       var nr = [...pro];
       var obj = {};
       narr.forEach((e, i) => {
         obj[i] = e;
       });
       nr[index].tasks = obj;
       setpro(nr);
        db.collection("workspaces").doc(id).update({
          projects: pro,
        });
        
    }
    const addNewTask =() => {
        var obj={
            task:'',
            owner:'',
            duedate: new Date().toLocaleDateString(),
            status:'',
            priority:''
        }
        var narr = [...tasks, obj];
        setTask(narr);
        var nr = [...pro];
        var obj ={}
        narr.forEach((e,i) => {
          obj[i] =e
        })
        nr[index].tasks = obj;
        setpro(nr);
        db.collection("workspaces").doc(id).update({
          projects: pro,
        });

    }
    return (
      <div>
        <div>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Task</Table.HeaderCell>
                <Table.HeaderCell>Owner</Table.HeaderCell>
                <Table.HeaderCell>Due date</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell>Priority</Table.HeaderCell>
                <Table.HeaderCell></Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {tasks !== [] ? (
                tasks.map((t, i) => (
                  <Table.Row key={i}>
                    <Table.Cell>
                      <Input
                        placeholder="enter task"
                        type="text"
                        value={tasks[i].task}
                        onChange={Change(i, "task")}
                      ></Input>
                    </Table.Cell>
                    <Table.Cell>
                      <Input
                        placeholder="enter task"
                        type="text"
                        value={tasks[i].owner}
                        onChange={Change(i, "owner")}
                      ></Input>
                    </Table.Cell>
                    <Table.Cell>
                      <DatePicker
                        dateFormat="yyyy/MM/dd"
                        placeholderText="Click to select a date"
                        selected={Date.parse(tasks[i].duedate)}
                        onChange={ChangeDate(i)}
                      ></DatePicker>
                    </Table.Cell>
                    <Table.Cell>
                      <select
                        placeholder="choose a status"
                        value={tasks[i].status}
                        onChange={Change(i, "status")}
                      >
                        <option>Paused</option>
                        <option>Progress</option>
                        <option>Completed</option>
                      </select>
                    </Table.Cell>
                    <Table.Cell>
                      <select
                        placeholder="choose a priority"
                        value={tasks[i].priority}
                        onChange={Change(i, "priority")}
                      >
                        <option>High</option>
                        <option>Medium</option>
                        <option>Low</option>
                      </select>
                    </Table.Cell>
                    <Table.Cell><Button
                      variant="outlined"
                      onClick={()=> {
                        // console.log(props);
                          props[0].history.push({
                            pathname:`/task/${index}/${i}`,
                            state:{
                              id : id,
                              
                            }
                          })
                      }}
                    >View</Button></Table.Cell>
                  </Table.Row>
                ))
              ) : (
                <div></div>
              )}
            </Table.Body>
          </Table>
          <Button
            variant="outlined"
            size="large"
            color="primary"
            onClick={addNewTask}
            style={{ padding: "1em", margin: "1em" }}
          >
            Add new task
          </Button>
        </div>
      </div>
    );
}


const Project = ( {pro, id, props}) => {
    const classes = useStyles();
    const [expanded, setExpanded] = useState(true);
    const [local, setlocal] = useState([]);
    var ref =null;
    React.useEffect(()=>{
        ref = db.collection('workspaces').doc(id);
        ref.get().then(async(snapshot)=>{
          if ( snapshot.exists ){
              var data = snapshot.data();
              if ( data.projects !== undefined)
              setlocal(data.projects);
              console.log(local);
          }
          else{
            setlocal([]);
            console.log('no content');
          }
        })
        
    },[id]);
    const handleClick = () => {
      setExpanded(!expanded);
    };

    const AddProject = () => {
      const obj = {
        name: "",
        approver: "",
        duedate: new Date().toLocaleDateString(),
        status: "",
        tasks:""
      };
      
      db.collection("workspaces")
        .doc(id)
        .update({
          projects: firebase.firestore.FieldValue.arrayUnion(
            obj
          ),
        });
        var nr = [...local , obj];
        setlocal(nr);
      console.log(local);
    };

    

    const handleChange = (t,i) => e => {
        var data = [ ...local];
        data[i][t] = e.target.value;
        setlocal(data);
        console.log(local)
        db.collection('workspaces').doc(id).update({
          projects : local
        })
    }
    const ChangeDate=i=> (date) => {
        var data= [...local];
        data[i]['duedate']=date;
        setlocal(data);
        db.collection("workspaces").doc(id).update({
          projects: local,
        });
    }
    return (
      <div>
        <div style={{ marginTop: "4em", padding: "2em" }}>
          <Button variant="outlined" size="large" onClick={AddProject}>
            Add Project
          </Button>
        </div>
        {local !== [] ? (
          local.map((e, i) => (
            <div
              key={i}
              style={{
                // display: "flex",
                // flexDirection: "row",
                justifyContent: "space-evenly",
                width: "100%",
              }}
            >
              <div>
                <strong>
                  <Input
                    placeholder="Project name"
                    value={local[i].name}
                    onChange={handleChange("name", i)}
                  ></Input>
                </strong>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleClick}
                  aria-label="show more"
                >
                  <ExpandMoreIcon
                    style={{ height: "30px", width: "30px", color: "black" }}
                  />
                </IconButton>

                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                    alignItems: "center",
                  }}
                >
                  <Item>
                    <Typography>Approver</Typography>
                    <Input
                      placeholder="Assign a approver"
                      type="email"
                      value={local[i].approver}
                      onChange={handleChange("approver", i)}
                    ></Input>
                  </Item>
                  <Item>
                    <Typography>Due Date</Typography>
                    <DatePicker
                      dateFormat="yyyy/MM/dd"
                      placeholderText="Click to select a date"
                      selected={Date.parse(local[i].duedate)}
                      onChange={ChangeDate(i)}
                    ></DatePicker>
                  </Item>
                  <Item>
                    <Typography>Status</Typography>
                    <select
                      placeholder="choose a status"
                      value={local[i].status}
                      onChange={handleChange("status", i)}
                    >
                      <option>Paused</option>
                      <option>On progress</option>
                      <option>Completed</option>
                    </select>
                  </Item>
                  {/* <Button variant="outlined" size="medium" rounded>
                    {" "}
                    View
                  </Button> */}
                </div>
                {expanded ? <Task id={id} index={i} props={props}/> : <div></div>}
                <Divider />
              </div>
            </div>
          ))
        ) : (
          <div></div>
        )}
      </div>
    );

}

const ProjectDetail = ({projects , callback , id ,props}) => {
    
    const classes = useStyles();

    return (
      <div>
        
        <Project pro={projects} id={id} props={props}/>
      </div>
    );
    
    
};

export default withRouter(ProjectDetail);